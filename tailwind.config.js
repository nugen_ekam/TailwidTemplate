module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        header_color: "#212121"
      },
      fontFamily: {
        dancing_script: "Dancing Script, cursive",
        great_vibes: "Great Vibes, cursive",
        lobster: "Lobster Two, cursive"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
